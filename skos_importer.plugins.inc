<?php

/**
 * @file
 * CTools plugins declarations.
 */

/**
 * Break out for skos_importer_skos_importer_plugins().
 */
function _skos_importer_skos_importer_plugins() {
  $path = drupal_get_path('module', 'skos_importer') . '/plugins';

  $info = array();
  $info['SKOSImporterPlugin'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'class' => 'SKOSImporterPlugin',
      'file' => 'SKOSImporterPlugin.inc',
      'path' => $path,
    ),
  );
  $info['SKOSImporterMissingPlugin'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'class' => 'SKOSImporterMissingPlugin',
      'file' => 'SKOSImporterPlugin.inc',
      'path' => $path,
    ),
  );
  $info['SKOSImporterFetcher'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'SKOSImporterPlugin',
      'class' => 'SKOSImporterFetcher',
      'file' => 'SKOSImporterFetcher.inc',
      'path' => $path,
    ),
  );
  $info['SKOSImporterParser'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'SKOSImporterPlugin',
      'class' => 'SKOSImporterParser',
      'file' => 'SKOSImporterParser.inc',
      'path' => $path,
    ),
  );
  $info['SKOSImporterTermParser'] = array(
    'name' => 'Term Parser',
    'description' => 'Parse terms.',
    'handler' => array(
      'parent' => 'SKOSImporterParser', // This is the key name, not the class name.
      'class' => 'SKOSImporterTermParser',
      'file' => 'SKOSImporterTermParser.inc',
      'path' => $path,
    ),
  );
  $info['SKOSImporterProcessor'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'SKOSImporterPlugin',
      'class' => 'SKOSImporterProcessor',
      'file' => 'SKOSImporterProcessor.inc',
      'path' => $path,
    ),
  );
  $info['SKOSImporterHTTPFetcher'] = array(
    'name' => 'HTTP Fetcher',
    'description' => 'Download content from a URL.',
    'handler' => array(
      'parent' => 'SKOSImporterFetcher', // This is the key name, not the class name.
      'class' => 'SKOSImporterHTTPFetcher',
      'file' => 'SKOSImporterHTTPFetcher.inc',
      'path' => $path,
    ),
  );
  $info['SKOSImporterFileFetcher'] = array(
    'name' => 'File upload',
    'description' => 'Upload content from a local file.',
    'handler' => array(
      'parent' => 'SKOSImporterFetcher',
      'class' => 'SKOSImporterFileFetcher',
      'file' => 'SKOSImporterFileFetcher.inc',
      'path' => $path,
    ),
  );
  $info['SKOSImporterTermProcessor'] = array(
    'name' => 'Taxonomy term processor',
    'description' => 'Create taxonomy terms.',
    'help' => 'Create taxonomy terms from parsed content.',
    'handler' => array(
      'parent' => 'SKOSImporterProcessor',
      'class' => 'SKOSImporterTermProcessor',
      'file' => 'SKOSImporterTermProcessor.inc',
      'path' => $path,
    ),
  );
  return $info;
}
