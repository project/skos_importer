<?php

/**
 * @file
 * Rules integration.
 */

/**
 * Implements hook_rules_event_info().
 */
function skos_importer_rules_event_info() {
  $info = array();
  $entity_type = 'taxonomy_term';
  $entity_info = entity_get_info($entity_type);

  foreach (skos_importer_load_all() as $importer) {
    $config = $importer->getConfig();
    $processor = skos_importer_plugin($config['processor']['plugin_key'], $importer->id);

    // It's possible to get SKOSImporterMissingPlugin here which will break things
    // since it doesn't implement SKOSImporterProcessor::entityType().
    if (!$processor instanceof SKOSImporterProcessor) {
      continue;
    }

    $label = isset($entity_info[$entity_type]['label']) ? $entity_info[$entity_type]['label'] : $entity_type;

    $info['skos_importer_import_'. $importer->id] = array(
      'label' => t('Before saving an item imported via @name.', array('@name' => $importer->config['name'])),
      'group' => t('SKOSImporter'),
      'variables' => array(
        $entity_type => array(
          'label' => t('Imported @label', array('@label' => $label)),
          'type' => $entity_type,
          // Saving is handled by skos_importer anyway (unless the skip action is used).
          'skip save' => TRUE,
        ),
      ),
      'access callback' => 'skos_importer_rules_access_callback',
    );
    // Add bundle information if the node processor is used.
    if ($processor instanceof SKOSImporterNodeProcessor) {
      $config = $processor->getConfig();
      $info['skos_importer_import_'. $importer->id]['variables'][$entity_type]['bundle'] = $config['content_type'];
    }
  }
  return $info;
}

/**
 * Implements of hook_rules_action_info().
 */
function skos_importer_rules_action_info() {
  return array(
    'skos_importer_skip_item' => array(
      'base' => 'skos_importer_action_skip_item',
      'label' => t('Skip import of term'),
      'group' => t('SKOSImporter'),
      'parameter' => array(
        'entity' => array('type' => 'entity', 'label' => t('The SKOS import item to be marked as skipped')),
      ),
      'access callback' => 'skos_importer_rules_access_callback',
    ),
  );
}

/**
 * Mark SKOS import item as skipped.
 */
function skos_importer_action_skip_item($entity_wrapper) {
  $entity = $entity_wrapper->value();
  if (isset($entity->skos_importer_item)) {
    $entity->skos_importer_item->skip = TRUE;
  }
}

/**
 * Help callback for the skip action.
 */
function skos_importer_action_skip_item_help() {
  return t("This action allows skipping certain terms during SKOS Importer processing, i.e. before an imported item is saved. Once this action is used on a item, the changes to the entity of the term are not saved.");
}

/**
 * Access callback for the SKOS Importer rules integration.
 */
function skos_importer_rules_access_callback() {
  return user_access('administer skos importer');
}
