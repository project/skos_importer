<?php

/**
 * @file
 * Contains all page callbacks, forms and theming functions for SKOS Importer
 * administrative pages.
 */

/**
 * Build overview of available configurations.
 */
function skos_importer_ui_overview_form($form, &$form_status) {
  $form = $form['enabled'] = $form['disabled'] = array();

  $form['#header'] = array(
    t('Name'),
    t('Description'),
    t('Attached to'),
    t('Status'),
    t('Operations'),
    t('Enabled'),
  );
  foreach (skos_importer_load_all(TRUE) as $importer) {
    $importer_form = array();
    $importer_form['name']['#markup'] = check_plain($importer->config['name']);
    $importer_form['description']['#markup'] = check_plain($importer->config['description']);
    if (empty($importer->config['content_type'])) {
      $importer_form['attached']['#markup'] = '[none]';
    }
    else {
      if (!$importer->disabled) {
        $importer_form['attached']['#markup'] = l(node_type_get_name($importer->config['content_type']), 'node/add/' . str_replace('_', '-', $importer->config['content_type']));
      }
      else {
        $importer_form['attached']['#markup'] = check_plain(node_type_get_name($importer->config['content_type']));
      }
    }

    if ($importer->export_type == EXPORT_IN_CODE) {
      $status = t('Default');
      $edit = t('Override');
      $delete = '';
    }
    elseif ($importer->export_type == EXPORT_IN_DATABASE) {
      $status = t('Normal');
      $edit = t('Edit');
      $delete = t('Delete');
    }
    elseif ($importer->export_type == (EXPORT_IN_CODE | EXPORT_IN_DATABASE)) {
      $status = t('Overridden');
      $edit = t('Edit');
      $delete = t('Revert');
    }
    $importer_form['status'] = array(
      '#markup' => $status,
    );
    if (!$importer->disabled) {
      $importer_form['operations'] = array(
        '#markup' =>
          l($edit, 'admin/structure/skos-importer/' . $importer->id) . ' | ' .
          //l(t('Export'), 'admin/structure/skos-importer/' . $importer->id . '/export') . ' | ' .
          l(t('Clone'), 'admin/structure/skos-importer/' . $importer->id . '/clone') .
          (empty($delete) ? '' :  ' | ' . l($delete, 'admin/structure/skos-importer/' . $importer->id . '/delete')),
      );
    }
    else {
      $importer_form['operations']['#markup'] = '&nbsp;';
    }

    $importer_form[$importer->id] = array(
      '#type' => 'checkbox',
      '#default_value' => !$importer->disabled,
      '#attributes' => array('class' => array('skos-importer-ui-trigger-submit')),
    );

    if ($importer->disabled) {
      $form['disabled'][$importer->id] = $importer_form;
    }
    else {
      $form['enabled'][$importer->id] = $importer_form;
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => array('skos-importer-ui-hidden-submit')),
  );
  return $form;
}

/**
 * Submit handler for skos_importer_ui_overview_form().
 */
function skos_importer_ui_overview_form_submit($form, &$form_state) {

  $disabled = array();
  foreach (skos_importer_load_all(TRUE) as $importer) {
    $disabled[$importer->id] = !$form_state['values'][$importer->id];
  }
  variable_set('default_skos_importer', $disabled);
  skos_importer_cache_clear();
}

/**
 * Create a new configuration.
 *
 * @param $form_state
 *  Form API form state array.
 * @param $from_importer
 *   SKOSImporterImporter object. If given, form will create a new importer as a copy
 *   of $from_importer.
 */
function skos_importer_ui_create_form($form, &$form_state, $from_importer = NULL) {
  $form['#attached']['js'][] = drupal_get_path('module', 'skos_importer_ui') . '/skos_importer_ui.js';
  $form['#from_importer'] = $from_importer;
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('A natural name for this configuration. You can always change this name later.'),
    '#required' => TRUE,
    '#maxlength' => 128,
  );
  $form['id'] = array(
    '#type' => 'machine_name',
    '#required' => TRUE,
    '#maxlength' => 128,
    '#machine_name' => array(
      'exists' => 'skos_importer_ui_importer_machine_name_exists',
    ),
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('A description of this configuration.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );
  return $form;
}

/**
 * Validation callback for the importer machine name field.
 */
function skos_importer_ui_importer_machine_name_exists($id) {
  if ($id == 'create') {
    // Create is a reserved path for the add importer form.
    return TRUE;
  }
  ctools_include('export');
  if (ctools_export_load_object('skos_importer_importer', 'conditions', array('id' => $id))) {
    return TRUE;
  }
}

/**
 * Validation handler for skos_importer_build_create_form().
 */
function skos_importer_ui_create_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['id'])) {
    $importer = skos_importer_importer($form_state['values']['id']);
    $importer->configFormValidate($form_state['values']);
  }
}

/**
 * Submit handler for skos_importer_build_create_form().
 */
function skos_importer_ui_create_form_submit($form, &$form_state) {
  // Create skos_importer
  $importer = skos_importer_importer($form_state['values']['id']);
  // If from_importer is given, copy its configuration.
  if (!empty($form['#from_importer'])) {
    $importer->copy($form['#from_importer']);
  }
  // In any case, we want to set this configuration's title and description.
  $importer->addConfig($form_state['values']);
  $importer->save();

  // Set a message and redirect to settings form.
  if (empty($form['#from_importer'])) {
    drupal_set_message(t('Your configuration has been created with default settings. If they do not fit your use case you can adjust them here.'));
  }
  else {
    drupal_set_message(t('A clone of the @name configuration has been created.', array('@name' => $form['#from_importer']->config['name'])));
  }
  $form_state['redirect'] = 'admin/structure/skos-importer/' . $importer->id;
  skos_importer_cache_clear();
}

/**
 * Delete configuration form.
 */
function skos_importer_ui_delete_form($form, &$form_state, $importer) {
  $form['#importer'] = $importer->id;
  if ($importer->export_type & EXPORT_IN_CODE) {
    $title = t('Would you really like to revert the importer @importer?', array('@importer' => $importer->config['name']));
    $button_label = t('Revert');
  }
  else {
    $title = t('Would you really like to delete the importer @importer?', array('@importer' => $importer->config['name']));
    $button_label = t('Delete');
  }
  return confirm_form(
    $form,
    $title,
    'admin/structure/skos-importer',
    t('This action cannot be undone.'),
    $button_label
  );
}

/**
 * Submit handler for skos_importer_ui_delete_form().
 */
function skos_importer_ui_delete_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/skos-importer';

  // Remove importer.
  skos_importer_importer($form['#importer'])->delete();

  // Clear cache, deleting a configuration may have an affect on menu tree.
  skos_importer_cache_clear();
}

/**
 * Edit configuration.
 */
function skos_importer_ui_edit_page($importer, $active = 'settings', $plugin_key = '') {
  return theme('skos_importer_ui_edit_page', array('form' => skos_importer_get_form($importer, 'configForm')));
}

/**
 * Walk the result of SKOSImporterParser::getMappingSources() or
 * SKOSImporterProcessor::getMappingTargets() and format them into
 * a Form API options array.
 */
function _skos_importer_ui_format_options($options) {
  $result = array();
  foreach ($options as $k => $v) {
    if (is_array($v) && !empty($v['name'])) {
      $result[$k] = $v['name'];
    }
    elseif (is_array($v)) {
      $result[$k] = $k;
    }
    else {
      $result[$k] = $v;
    }
  }
  asort($result);
  return $result;
}

/**
 * Theme skos_importer_ui_overview_form().
 */
function theme_skos_importer_ui_overview_form($variables) {
  $form = $variables['form'];
  drupal_add_js(drupal_get_path('module', 'skos_importer_ui') . '/skos_importer_ui.js');
  drupal_add_css(drupal_get_path('module', 'skos_importer_ui') . '/skos_importer_ui.css');

  // Iterate through all importers and build a table.
  $rows = array();
  foreach (array('enabled', 'disabled') as $type) {
    if (isset($form[$type])) {
      foreach (element_children($form[$type]) as $id) {
        $row = array();
        foreach (element_children($form[$type][$id]) as $col) {
          $row[$col] = array(
            'data' => drupal_render($form[$type][$id][$col]),
            'class' => array($type),
          );
        }
        $rows[] = array(
          'data' => $row,
          'class' => array($type),
        );
      }
    }
  }

  $output = theme('table', array(
    'header' => $form['#header'],
    'rows' => $rows,
    'attributes' => array('class' => array('skos-importer-admin-importers')),
    'empty' => t('No importers available.'),
  ));

  if (!empty($rows)) {
    $output .= drupal_render_children($form);
  }

  return $output;
}

/**
 * Theme skos_importer_ui_edit_page().
 */
function theme_skos_importer_ui_edit_page($variables) {
  drupal_add_css(drupal_get_path('module', 'skos_importer_ui') . '/skos_importer_ui.css');

  // Outer wrapper.
  $output = '<div class="skos-importer-settings clear-block">';

  // Build configuration space.
  $output .= '<div class="configuration">';
  $output .= drupal_render($variables['form']);
  $output .= '</div>';

  $output .= '</div>';

  return $output;
}
