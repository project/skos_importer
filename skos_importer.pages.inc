<?php

/**
 * @file
 * Menu callbacks, form callbacks and helpers.
 */

/**
 * Render a page of available importers.
 */
/**
 * TODO: check if all config data is given
 */
function skos_importer_page() {
  $rows = array();
  if ($importers = skos_importer_load_all()) {
    foreach ($importers as $importer) {
      if ($importer->disabled) {
        continue;
      }
      if (!(user_access('import ' . $importer->id . ' skos importer') || user_access('administer skos importer'))) {
        continue;
      }
      $link = 'import/' . $importer->id;
      $title = $importer->config['name'];
      $date = $importer->getLastImportDate();
      $date = empty($date) ? t('Has not yet imported.') : format_date($importer->getLastImportDate(), 'custom', 'M j, Y');
      $rows[] = array(
        l($title, $link),
        check_plain($importer->config['description']),
        $date,
      );
    }
  }
  if (empty($rows)) {
    drupal_set_message(t('There are no importers, go to <a href="@importers">SKOS importers</a> to create one or enable an existing one.', array('@importers' => url('admin/structure/skos-importer'))));
  }
  $header = array(
    t('Import'),
    t('Description'),
    t('Last import'),
  );
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Render a SKOS Importer import form on import/[config] pages.
 */
/**
 * TODO: check if all config data is given
 */
function skos_importer_import_form($form, &$form_state, $importer_id) {
  $source = skos_importer_source($importer_id);

  $form = array();
  $form['#importer_id'] = $importer_id;
  $form['source_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
    '#tree' => TRUE,
    '#value' => skos_importer_source_status($source),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );
  $progress = $source->progressImporting();
  if ($progress !== SKOSIMPORTER_BATCH_COMPLETE) {
    $form['submit']['#disabled'] = TRUE;
    $form['submit']['#value'] =
      t('Importing (@progress %)', array('@progress' => number_format(100 * $progress, 0)));
  }
  return $form;
}

/**
 * Validation handler for node forms and skos_importer_import_form().
 */
function skos_importer_import_form_validate($form, &$form_state) {
  skos_importer_source($form['#importer_id'])->configFormValidate($form_state['values']['skos_importer']);
}

/**
 * Submit handler for skos_importer_import_form().
 */
function skos_importer_import_form_submit($form, &$form_state) {
  // Save source and import.
  $source = skos_importer_source($form['#importer_id']);

  // Start the import.
  $source->startImport();

  // Add to schedule, make sure importer is scheduled, too.
  // TODO: fuer spaeter aufgehoben :)
//  $source->schedule();
//  $source->importer->schedule();
}

/**
 * Submit handler for skos_importer_import_tab_form().
 */
function skos_importer_import_tab_form_submit($form, &$form_state) {
  $form_state['redirect'] = $form['#redirect'];
  skos_importer_source($form['#importer_id'])->startImport();
}

/**
 * Render a SKOS Importer delete form.
 *
 * Used on both node pages and configuration pages.
 * Therefore $node may be missing.
 */
function skos_importer_delete_tab_form($form, &$form_state, $importer_id, $node = NULL) {
  $source = skos_importer_source($importer_id);
  $form['#redirect'] = 'import/' . $source->id;

  // Form cannot pass on source object.
  $form['#importer_id'] = $source->id;
  $form['source_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
    '#tree' => TRUE,
    '#value' => skos_importer_source_status($source),
  );
  $form = confirm_form($form, t('Delete all items from source?'), $form['#redirect'], '', t('Delete'), t('Cancel'), 'confirm taxonomy update');
  $progress = $source->progressClearing();
  if ($progress !== SKOSIMPORTER_BATCH_COMPLETE) {
    $form['actions']['submit']['#disabled'] = TRUE;
    $form['actions']['submit']['#value'] =
      t('Deleting (@progress %)', array('@progress' => number_format(100 * $progress, 0)));
  }
  return $form;
}

/**
 * Submit handler for skos_importer_delete_tab_form().
 */
function skos_importer_delete_tab_form_submit($form, &$form_state) {
  $form_state['redirect'] = $form['#redirect'];
  skos_importer_source($form['#importer_id'])->startClear();
}

/**
 * Render a SKOS Importer unlock form.
 *
 * Used on both node pages and configuration pages.
 * Therefore $node may be missing.
 */
function skos_importer_unlock_tab_form($form, &$form_state, $importer_id, $node = NULL) {
  $source = skos_importer_source($importer_id);
  $form['#redirect'] = 'import/' . $source->id;

  // Form cannot pass on source object.
  $form['#importer_id'] = $source->id;
  $form['source_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
    '#tree' => TRUE,
    '#value' => skos_importer_source_status($source),
  );
  $form = confirm_form($form, t('Unlock this importer?'), $form['#redirect'], '', t('Delete'), t('Cancel'), 'confirm taxonomy update');
  if ($source->progressImporting() == SKOSIMPORTER_BATCH_COMPLETE && $source->progressClearing() == SKOSIMPORTER_BATCH_COMPLETE) {
    $form['source_locked'] = array(
      '#type' => 'markup',
      '#title' => t('Not Locked'),
      '#tree' => TRUE,
      '#markup' => t('This importer is not locked, therefore it cannot be unlocked.'),
    );
    $form['actions']['submit']['#disabled'] = TRUE;
    $form['actions']['submit']['#value'] = t('Unlock (disabled)');
  }
  else {
    $form['actions']['submit']['#value'] = t('Unlock');
  }
  return $form;
}

/**
 * Form submit handler. Resets all Skos Importer state.
 */
function skos_importer_unlock_tab_form_submit($form, &$form_state) {
  drupal_set_message(t('Import Unlocked'));
  $form_state['redirect'] = $form['#redirect'];
}

function skos_importer_log_table($importer_id) {

  $severityLevels = watchdog_severity_levels();
    
  $logs = db_query(
    'SELECT l.log_time, l.message, l.severity, l.import_id, i.start_time, u.name as username
    FROM skos_importer_log l, skos_importer_imports i, users u
    WHERE l.import_id = i.id
    AND i.uid = u.uid
    AND i.importer_id = \'' . $importer_id . '\'
    ORDER BY l.log_time DESC
    LIMIT 50');
    
  $rows = array();
  
  $import_id = -1;
  if($logs->rowCount() > 0)
  {
    foreach ($logs as $log)
    {
      if($import_id != $log->import_id)
      {
        $rows[] = array('data' => array(array('data' => '<h2>Import #' . $log->import_id . ' by ' . $log->username . ' started on ' . date('Y-M-d, H:m:s', $log->start_time) . '</h2>', 'colspan' => 4, 'style' => 'text-align:center')));
        $import_id = $log->import_id;
      }
      $rows[] = array(date('Y-M-d, H:m:s', $log->log_time), $log->message, $severityLevels[$log->severity]);
    }

    return theme('table', array('header' => array('Log time', 'Message', 'Severity'), 'rows' => $rows));
  }
  else
  {
    return '<h2>No logs available for this importer</h2>';
  }
}

/**
 * Handle a fetcher callback.
 */
function skos_importer_fetcher_callback($importer) {
  if ($importer instanceof SKOSImporterImporter) {
    try {
      return $importer->fetcher->request();
    }
    catch (Exception $e) {
      // Do nothing.
    }
  }
  drupal_access_denied();
}

/**
 * Renders a status display for a source.
 */
function skos_importer_source_status($source) {
  $progress_importing = $source->progressImporting();
  $v = array();
  if ($progress_importing != SKOSIMPORTER_BATCH_COMPLETE) {
    $v['progress_importing'] = $progress_importing;
  }
  $progress_clearing = $source->progressClearing();
  if ($progress_clearing != SKOSIMPORTER_BATCH_COMPLETE) {
    $v['progress_clearing'] = $progress_clearing;
  }
  $v['imported'] = $source->imported;
  $v['count'] = $source->termCount();
  if (!empty($v)) {
    return theme('skos_importer_source_status', $v);
  }
}

/**
 * Themes a status display for a source.
 */
function theme_skos_importer_source_status($v) {
  $output = '<div class="info-box skos-importer-source-status">';
  $items = array();
  if ($v['progress_importing']) {
    $progress = number_format(100.0 * $v['progress_importing'], 0);
    $items[] = t('Importing - @progress % complete.', array('@progress' => $progress));
  }
  if ($v['progress_clearing']) {
    $progress = number_format(100.0 * $v['progress_clearing'], 0);
    $items[] = t('Deleting terms - @progress % complete.', array('@progress' => $progress));
  }
  if (!count($items)) {
    if ($v['count']) {
      if ($v['imported']) {
        $items[] = t('Last import: @ago ago.', array('@ago' => format_interval(REQUEST_TIME - $v['imported'], 1)));
      }
      $items[] = t('@count imported terms total.', array('@count' => $v['count']));
    }
    else {
      $items[] = t('No imported terms.');
    }
  }
  $output .= theme('item_list', array('items' => $items));
  $output .= '</div>';
  return $output;
}

/**
 * Theme upload widget.
 */
function theme_skos_importer_upload($variables) {
  $element = $variables['element'];
  drupal_add_css(drupal_get_path('module', 'skos_importer') . '/skos_importer.css');
  _form_set_class($element, array('form-file'));
  $description = '';
  if (!empty($element['#file_info'])) {
    $file = $element['#file_info'];
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $description .= '<div class="file-info">';
    $description .= '<div class="file-name">';
    $description .= l($file->filename, $wrapper->getExternalUrl());
    $description .= '</div>';
    $description .= '<div class="file-size">';
    $description .= format_size($file->filesize);
    $description .= '</div>';
    $description .= '<div class="file-mime">';
    $description .= check_plain($file->filemime);
    $description .= '</div>';
    $description .= '</div>';
  }
  $description .= '<div class="file-upload">';
  $description .= '<input type="file" name="' . $element['#name'] . '"' . ($element['#attributes'] ? ' ' . drupal_attributes($element['#attributes']) : '') . ' id="' . $element['#id'] . '" size="' . $element['#size'] . "\" />\n";
  $description .= '</div>';
  $element['#description'] = $description;

  // For some reason not unsetting #title leads to printing the title twice.
  unset($element['#title']);
  return theme('form_element', $element);
}
