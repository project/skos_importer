<?php

/**
 * @file
 * Definition of SKOSImporterSourceInterface and SKOSImporterSource class.
 */

/**
 * Distinguish exceptions occuring when handling locks.
 */
class SKOSImporterLockException extends Exception {}

/**
 * Denote a import or clearing stage. Used for multi page processing.
 */
define('SKOSIMPORTER_START', 'start_time');
define('SKOSIMPORTER_FETCH', 'fetch');
define('SKOSIMPORTER_PARSE', 'parse');
define('SKOSIMPORTER_PROCESS', 'process');
define('SKOSIMPORTER_PROCESS_CLEAR', 'process_clear');

/**
 * Declares an interface for a class that defines default values and form
 * descriptions for a SKOSImporterSource.
 */
interface SKOSImporterSourceInterface {

  /**
   * Crutch: for ease of use, we implement SKOSImporterSourceInterface for every
   * plugin, but then we need to have a handle which plugin actually implements
   * a source.
   *
   * @see SKOSImporterPlugin class.
   *
   * @return
   *   TRUE if a plugin handles source specific configuration, FALSE otherwise.
   */
  public function hasSourceConfig();

  /**
   * Return an associative array of default values.
   */
  public function sourceDefaults();

  /**
   * Return a Form API form array that defines a form configuring values. Keys
   * correspond to the keys of the return value of sourceDefaults().
   */
  public function sourceForm($source_config);

  /**
   * Validate user entered values submitted by sourceForm().
   */
  public function sourceFormValidate(&$source_config);

  /**
   * A source is being saved.
   */
  public function sourceSave(SKOSImporterSource $source);

  /**
   * A source is being deleted.
   */
  public function sourceDelete(SKOSImporterSource $source);
}

/**
 * Status of an import or clearing operation on a source.
 */
class SKOSImporterState {
  /**
   * Floating point number denoting the progress made. 0.0 meaning no progress
   * 1.0 = SKOSIMPORTER_BATCH_COMPLETE meaning finished.
   */
  public $progress;

  /**
   * Used as a pointer to store where left off. Must be serializable.
   */
  public $pointer;

  /**
   * Natural numbers denoting more details about the progress being made.
   */
  public $total;
  public $created;
  public $updated;
  public $deleted;
  public $skipped;
  public $failed;

  /**
   * Constructor, initialize variables.
   */
  public function __construct() {
    $this->progress = SKOSIMPORTER_BATCH_COMPLETE;
    $this->total =
    $this->created =
    $this->updated =
    $this->deleted =
    $this->skipped =
    $this->failed = 0;
  }

  /**
   * Safely report progress.
   *
   * When $total == $progress, the state of the task tracked by this state is
   * regarded to be complete.
   *
   * Handles the following cases gracefully:
   *
   * - $total is 0
   * - $progress is larger than $total
   * - $progress approximates $total so that $finished rounds to 1.0
   *
   * @param $total
   *   A natural number that is the total to be worked off.
   * @param $progress
   *   A natural number that is the progress made on $total.
   */
  public function progress($total, $progress) {
    if ($progress > $total) {
      $this->progress = SKOSIMPORTER_BATCH_COMPLETE;
    }
    elseif ($total) {
      $this->progress = $progress / $total;
      if ($this->progress == SKOSIMPORTER_BATCH_COMPLETE && $total != $progress) {
        $this->progress = 0.99;
      }
    }
    else {
      $this->progress = SKOSIMPORTER_BATCH_COMPLETE;
    }
  }
}

/**
 * This class encapsulates a source of a Skos Importer. It stores where the Skos Importer can be
 * found and how to import it.
 *
 * Information on how to import a Skos Importer is encapsulated in a SKOSImporterImporter object
 * which is identified by the common id of the SKOSImporterSource and the
 * SKOSImporterImporter. More than one SKOSImporterSource can use the same SKOSImporterImporter
 * therefore a SKOSImporterImporter never holds a pointer to a SKOSImporterSource object, nor
 * does it hold any other information for a particular SKOSImporterSource object.
 *
 * Classes extending SKOSImporterPlugin can implement a sourceForm to expose
 * configuration for a SKOSImporterSource object. This is for instance how SKOSImporterFetcher
 * exposes a text field for a Skos Importer URL or how SKOSImporterCSVParser exposes a select
 * field for choosing between colon or semicolon delimiters.
 *
 * As with SKOSImporterImporter, the idea with SKOSImporterSource is that it can be used
 * without actually saving the object to the database.
 */
class SKOSImporterSource extends SKOSImporterConfigurable {

  // Contains the node id of the Skos Importer this source info object is attached to.
  // Equals 0 if not attached to any node - i. e. if used on a
  // standalone import form within SKOS Importer or by other API users.

  // The SKOSImporterImporter object that this source is expected to be used with.
  protected $importer;

  // A SKOSImporterSourceState object holding the current import/clearing state of this
  // source.
  protected $state;

  // Fetcher result, used to cache fetcher result when batching.
  protected $fetcher_result;

  // Timestamp when this source was imported the last time.
  protected $imported;

  /**
   * Instantiate a unique object per class/importer_id. Don't use
   * directly, use skos_importer_source() instead.
   */
  public static function instance($importer_id) {
    $importer_exists = db_select('skos_importer', 'si')
      ->fields('si', array('id'))
      ->condition('id', $importer_id)
      ->execute()
      ->rowCount();

    if ($importer_exists) {
      $class = variable_get('skos_importer_source_class', 'SKOSImporterSource');
      static $instances = array();
      if (!isset($instances[$class][$importer_id])) {
        $instances[$class][$importer_id] = new $class($importer_id);
      }
      return $instances[$class][$importer_id];
    }
    else {
      return NULL;
    }
  }

  /**
   * Constructor.
   */
  protected function __construct($importer_id) {
    $this->importer = skos_importer_importer($importer_id);
    parent::__construct($importer_id);
    $this->load();
  }

  /**
   * Returns the SKOSImporterImporter object that this source is expected to be used with.
   */
  public function importer() {
    return $this->importer;
  }

  /**
   * Preview = fetch and parse a source.
   *
   * @return
   *   SKOSImporterParserResult object.
   *
   * @throws
   *   Throws Exception if an error occurs when fetching or parsing.
   */
  public function preview() {
    $result = $this->importer->fetcher->fetch($this);
    $result = $this->importer->parser->parse($this, $result);
    module_invoke_all('skos_importer_after_parse', $this, $result);
    return $result;
  }

  /**
   * Start importing a source.
   *
   * This method starts an import job. Depending on the configuration of the
   * importer of this source, a Batch API job or a background job with Job
   * Scheduler will be created.
   *
   * @throws Exception
   *   If processing in background is enabled, the first batch chunk of the
   *   import will be executed on the current page request. This means that this
   *   method may throw the same exceptions as SKOSImporterSource::import().
   */
  public function startImport() {
    $config = $this->importer->getConfig();
    if ($config['process_in_background']) {
      //$this->startBackgroundJob('import');
      $this->startBatchAPIJob(t('Importing'), 'import');
    }
    else {
      $this->startBatchAPIJob(t('Importing'), 'import');
    }
  }

  /**
   * Start deleting all imported items of a source.
   *
   * This method starts a clear job. Depending on the configuration of the
   * importer of this source, a Batch API job or a background job with Job
   * Scheduler will be created.
   *
   * @throws Exception
   *   If processing in background is enabled, the first batch chunk of the
   *   clear task will be executed on the current page request. This means that
   *   this method may throw the same exceptions as SKOSImporterSource::clear().
   */
  public function startClear() {
    $config = $this->importer->getConfig();
    if ($config['process_in_background']) {
      $this->startBackgroundJob('clear');
    }
    else {
      $this->startBatchAPIJob(t('Deleting'), 'clear');
    }
  }

  /**
   * Schedule all periodic tasks for this source.
   */
  public function schedule() {
    $this->scheduleImport();
  }

  /**
   * Schedule periodic or background import tasks.
   */
  public function scheduleImport() {
    // Check whether any fetcher is overriding the import period.
    $period = $this->importer->config['import_period'];
    $fetcher_period = $this->importer->fetcher->importPeriod($this);
    if (is_numeric($fetcher_period)) {
      $period = $fetcher_period;
    }
    $period = $this->progressImporting() === SKOSIMPORTER_BATCH_COMPLETE ? $period : 0;
    $job = array(
      'type' => $this->id,
      'id' => $this->id,
      // Schedule as soon as possible if a batch is active.
      'period' => $period,
      'periodic' => TRUE,
    );
    if ($period != SKOSIMPORTER_SCHEDULE_NEVER) {
      JobScheduler::get('skos_importer_source_import')->set($job);
    }
    else {
      JobScheduler::get('skos_importer_source_import')->remove($job);
    }
  }

  /**
   * Schedule background clearing tasks.
   */
  public function scheduleClear() {
    $job = array(
      'type' => $this->id,
      'id' => $this->id,
      'period' => 0,
      'periodic' => TRUE,
    );
    // Remove job if batch is complete.
    if ($this->progressClearing() === SKOSIMPORTER_BATCH_COMPLETE) {
      JobScheduler::get('skos_importer_source_clear')->remove($job);
    }
    // Schedule as soon as possible if batch is not complete.
    else {
      JobScheduler::get('skos_importer_source_clear')->set($job);
    }
  }

  /**
   * Import a source: execute fetching, parsing and processing stage.
   *
   * This method only executes the current batch chunk, then returns. If you are
   * looking to import an entire source, use SKOSImporterSource::startImport() instead.
   *
   * @return
   *   SKOSIMPORTER_BATCH_COMPLETE if the import process finished. A decimal between
   *   0.0 and 0.9 periodic if import is still in progress.
   *
   * @throws
   *   Throws Exception if an error occurs when importing.
   */
  public function import($use_batch = FALSE) {
    $this->acquireLock();
    try {
      // If fetcher result is empty, we are starting a new import, log.
      if (empty($this->fetcher_result)) {
        $this->state[SKOSIMPORTER_START] = time();
      }
      
      global $user;
      db_insert('skos_importer_imports')
      ->fields(array(
        'importer_id' => $this->importer->id,
        'start_time' => $this->state[SKOSIMPORTER_START],
        'end_time' => NULL,
        'uid' => (($user->uid) ? $user->uid : 0),
      ))
      ->execute();

      // Fetch.
      if (empty($this->fetcher_result) || SKOSIMPORTER_BATCH_COMPLETE == $this->progressParsing()) {
        $this->fetcher_result = $this->importer->fetcher->fetch($this);
        // Clean the parser's state, we are parsing an entirely new file.
        unset($this->state[SKOSIMPORTER_PARSE]);
      }

      // Parse.
      $parser_result = $this->importer->parser->parse($this, $this->fetcher_result);
      module_invoke_all('skos_importer_after_parse', $this, $parser_result);
    }
    catch (Exception $e) {
      var_dump($e); exit;
      // Do nothing.
    }
    
    // Batched processing.
    if ($use_batch) {
      $batch_operations = array();
      
      $batch_item_count = 50;
      $start_index = 0;
      while (count($parser_result->items) > $start_index) {
        $parser_result_part = clone $parser_result;
        $parser_result_part->items = array_slice($parser_result->items, $start_index, ((count($parser_result->items) >= ($start_index + $batch_item_count)) ? $batch_item_count : (count($parser_result->items) - $start_index)));
        
        $batch_operations[] = array('skos_importer_process_batch', array($this->id, $parser_result_part, $this->state));
        
        $start_index += $batch_item_count;
      }
      
      $batch = array(
        'title' => t('Importing'),
        'operations' => $batch_operations,
        'finished' => 'skos_importer_process_batch_complete',
        'progress_message' => '',
      );
      batch_set($batch);
    }
    else {
      try {
        // Normal processing.
        $processor_result = $this->importer->processor->process($this, $parser_result, $this->state);
        return $this->process_complete($processor_result['state']);
      }
      catch (Exception $e) {
        // Do nothing.
      }
      
      return $this->process_complete($this->state);
    }
  }
  
  public function process_complete($state = NULL) {
    if (!is_null($state)) {
      $this->importer->processor->finishProcessing($this, $state);
    }
    
    $this->releaseLock();

    // Clean up.
    $result = $this->progressImporting();
    if ($result == SKOSIMPORTER_BATCH_COMPLETE || isset($e)) {
      $this->imported = time();
      
      db_update('skos_importer_imports')
      ->fields(array(
        'end_time' => $this->imported,
      ))
      ->condition('importer_id', $this->importer->id)
      ->condition('start_time', $state[SKOSIMPORTER_START])
      ->execute();
      
      $this->log('import', 'Imported in ' . ($this->imported - $state[SKOSIMPORTER_START]) . ' s', WATCHDOG_INFO);
      module_invoke_all('skos_importer_after_import', $this);
      unset($this->fetcher_result, $this->state);
    }
    $this->save();
    if (isset($e)) {
      throw $e;
    }
    return $result;
  }

  /**
   * Remove all terms from a taxonomy.
   *
   * This method only executes the current batch chunk, then returns. If you are
   * looking to delete all items of a source, use SKOSImporterSource::startClear()
   * instead.
   *
   * @return
   *   SKOSIMPORTER_BATCH_COMPLETE if the clearing process finished. A decimal between
   *   0.0 and 0.9 periodic if clearing is still in progress.
   *
   * @throws
   *   Throws Exception if an error occurs when clearing.
   */
  public function clear() {
    $this->acquireLock();
    try {
      $this->importer->fetcher->clear($this);
      $this->importer->parser->clear($this);
      $this->importer->processor->clear($this);
    }
    catch (Exception $e) {
      // Do nothing.
    }
    $this->releaseLock();

    // Clean up.
    $result = $this->progressClearing();
    if ($result == SKOSIMPORTER_BATCH_COMPLETE || isset($e)) {
      module_invoke_all('skos_importer_after_clear', $this);
      unset($this->state);
    }
    $this->save();
    if (isset($e)) {
      throw $e;
    }
    return $result;
  }

  /**
   * Report progress as float between 0 and 1. 1 = SKOSIMPORTER_BATCH_COMPLETE.
   */
  public function progressParsing() {
    return $this->state(SKOSIMPORTER_PARSE)->progress;
  }

  /**
   * Report progress as float between 0 and 1. 1 = SKOSIMPORTER_BATCH_COMPLETE.
   */
  public function progressImporting() {
    $fetcher = $this->state(SKOSIMPORTER_FETCH);
    $parser = $this->state(SKOSIMPORTER_PARSE);
    if ($fetcher->progress == SKOSIMPORTER_BATCH_COMPLETE && $parser->progress == SKOSIMPORTER_BATCH_COMPLETE) {
      return SKOSIMPORTER_BATCH_COMPLETE;
    }
    // Fetching envelops parsing.
    // @todo: this assumes all fetchers neatly use total. May not be the case.
    $fetcher_fraction = $fetcher->total ? 1.0 / $fetcher->total : 1.0;
    $parser_progress = $parser->progress * $fetcher_fraction;
    $result = $fetcher->progress - $fetcher_fraction + $parser_progress;
    if ($result == SKOSIMPORTER_BATCH_COMPLETE) {
      return 0.99;
    }
    return $result;
  }

  /**
   * Report progress on clearing.
   */
  public function progressClearing() {
    return $this->state(SKOSIMPORTER_PROCESS_CLEAR)->progress;
  }

  /**
   * Return a state object for a given stage. Lazy instantiates new states.
   *
   * @todo Rename getConfigFor() accordingly to config().
   *
   * @param $stage
   *   One of SKOSIMPORTER_FETCH, SKOSIMPORTER_PARSE, SKOSIMPORTER_PROCESS or SKOSIMPORTER_PROCESS_CLEAR.
   *
   * @return
   *   The SKOSImporterState object for the given stage.
   */
  public function state($stage) {
    if (!is_array($this->state)) {
      $this->state = array();
    }
    if (!isset($this->state[$stage])) {
      $this->state[$stage] = new SKOSImporterState();
    }
    return $this->state[$stage];
  }

  /**
   * Count items imported by this source.
   */
  public function termCount() {
    return $this->importer->processor->termCount($this);
  }

  /**
   * Save configuration.
   */
  public function save() {
    // Alert implementers of SKOSImporterSourceInterface to the fact that we're saving.
    foreach ($this->importer->plugin_types as $type) {
      $this->importer->$type->sourceSave($this);
    }
    $config = $this->getConfig();

    // Store the source property of the fetcher in a separate column so that we
    // can do fast lookups on it.
    $source = '';
    if (isset($config[get_class($this->importer->fetcher)]['source'])) {
      $source = $config[get_class($this->importer->fetcher)]['source'];
    }
    
    db_update('skos_importer')
    ->fields(array(
      'import_state' => isset($this->state) ? $this->state : FALSE,
      'last_import' => $this->imported,
    ))
    ->condition('id', $this->id)
    ->execute();
  }

  /**
   * Load configuration and unpack.
   *
   * @todo Patch CTools to move constants from export.inc to ctools.module.
   */
   /**
    * TODO: fix sources (load)
    */
  public function load() {
    if ($record = db_query("SELECT last_import, config FROM {skos_importer} WHERE id = :id", array(':id' => $this->id))->fetchObject()) {
      // While SKOSImporterSource cannot be exported, we still use CTool's export.inc
      // export definitions.
      //ctools_include('export');
      //$this->export_type = EXPORT_IN_DATABASE;
      $this->imported = $record->last_import;
      $this->config = unserialize($record->config);
    }
  }

  /**
   * Delete configuration. Removes configuration information
   * from database, does not delete configuration itself.
   */
  public function delete() {
    // Alert implementers of SKOSImporterSourceInterface to the fact that we're
    // deleting.
    foreach ($this->importer->plugin_types as $type) {
      $this->importer->$type->sourceDelete($this);
    }
  }

  /**
   * Only return source if configuration is persistent and valid.
   *
   * @see SKOSImporterConfigurable::existing().
   */
  public function existing() {
    // Ensure that importer is persistent (= defined in code or DB).
    // Ensure that source is persistent (= defined in DB).
    $this->importer->existing();
    return parent::existing();
  }

  /**
   * Returns the configuration for a specific client class.
   *
   * @param SKOSImporterSourceInterface $client
   *   An object that is an implementer of SKOSImporterSourceInterface.
   *
   * @return
   *   An array stored for $client.
   */
  public function getConfigFor(SKOSImporterSourceInterface $client) {
    $class = get_class($client);
    return isset($this->config[$class]) ? $this->config[$class] : $client->sourceDefaults();
  }

  /**
   * Sets the configuration for a specific client class.
   *
   * @param SKOSImporterSourceInterface $client
   *   An object that is an implementer of SKOSImporterSourceInterface.
   * @param $config
   *   The configuration for $client.
   *
   * @return
   *   An array stored for $client.
   */
  public function setConfigFor(SKOSImporterSourceInterface $client, $config) {
    $this->config[get_class($client)] = $config;
  }

  /**
   * Return defaults for SKOS importer source configuration.
   */
  public function configDefaults() {
    // Collect information from plugins.
    $defaults = array();
    foreach ($this->importer->plugin_types as $type) {
      if ($this->importer->$type->hasSourceConfig()) {
        $defaults[get_class($this->importer->$type)] = $this->importer->$type->sourceDefaults();
      }
    }
    return $defaults;
  }

  /**
   * Writes to skos_importer log.
   */
  public function log($type, $message, $severity = WATCHDOG_NOTICE) {
    skos_importer_log($this->id, $type, $message, $severity);
  }

  /**
   * Background job helper. Starts a background job using Job Scheduler.
   *
   * Execute the first batch chunk of a background job on the current page load,
   * moves the rest of the job processing to a cron powered background job.
   *
   * Executing the first batch chunk is important, otherwise, when a user
   * submits a source for import or clearing, we will leave her without any
   * visual indicators of an ongoing job.
   *
   * @see SKOSImporterSource::startImport().
   * @see SKOSImporterSource::startClear().
   *
   * @param $method
   *   Method to execute on importer; one of 'import' or 'clear'.
   *
   * @throws Exception $e
   */
  protected function startBackgroundJob($method) {
    if (SKOSIMPORTER_BATCH_COMPLETE != $this->$method()) {
      $job = array(
        'type' => $this->id,
        'id' => $this->id,
        'period' => 0,
        'periodic' => FALSE,
      );
      JobScheduler::get("skos_importer_source_{$method}")->set($job);
    }
  }

  /**
   * Batch API helper. Starts a Batch API job.
   *
   * @see SKOSImporterSource::startImport().
   * @see SKOSImporterSource::startClear().
   * @see skos_importer_batch()
   *
   * @param $title
   *   Title to show to user when executing batch.
   * @param $method
   *   Method to execute on importer; one of 'import' or 'clear'.
   */
  protected function startBatchAPIJob($title, $method) {
    if ($method != 'import') {
      $batch = array(
        'title' => $title,
        'operations' => array(
          array('skos_importer_batch', array($method, $this->id)),
        ),
        'progress_message' => '',
      );
      batch_set($batch);
    }
    // Importing only uses batches for the processing.
    else {
      $this->import(TRUE);
    }
  }

  /**
   * Acquires a lock for this source.
   *
   * @throws SKOSImporterLockException
   *   If a lock for the requested job could not be acquired.
   */
  protected function acquireLock() {
    if (!lock_acquire("skos_importer_source_{$this->id}", 60.0)) {
      throw new SKOSImporterLockException(t('Cannot acquire lock for source @id.', array('@id' => $this->id)));
    }
  }

  /**
   * Releases a lock for this source.
   */
  protected function releaseLock() {
    lock_release("skos_importer_source_{$this->id}");
  }
}
