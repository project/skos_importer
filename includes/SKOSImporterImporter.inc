<?php

/**
 * @file
 * SKOSImporterImporter class and related.
 */
 
/**
 * A SKOSImporterImporter object describes how an external source should be fetched,
 * parsed and processed. SKOS Importer can manage an arbitrary amount of importers.
 *
 * A SKOSImporterImporter holds a pointer to a SKOSImporterFetcher, a SKOSImporterParser and a
 * SKOSImporterProcessor plugin. It further contains the configuration for itself and
 * each of the three plugins.
 *
 * Its most important responsibilities are configuration management, interfacing
 * with the job scheduler and expiring of all items produced by this
 * importer.
 *
 * When a SKOSImporterImporter is instantiated, it loads its configuration. Then it
 * instantiates one fetcher, one parser and one processor plugin depending on
 * the configuration information. After instantiating them, it sets them to
 * the configuration information it holds for them.
 */
class SKOSImporterImporter extends SKOSImporterConfigurable {

  // Every SKOS Importer has a fetcher, a parser and a processor.
  // These variable names match the possible return values of
  // SKOSImporterPlugin::typeOf().
  protected $fetcher, $parser, $processor;

  // This array defines the variable names of the plugins above.
  protected $plugin_types = array('fetcher', 'parser', 'processor');

  protected $imported;

  /**
   * Instantiate class variables, initialize and configure
   * plugins.
   */
  protected function __construct($id) {
    parent::__construct($id);

    // Try to load information from database.
    $this->load();

    $this->getLastImportDate();

    // Instantiate fetcher, parser and processor, set their configuration if
    // stored info is available.
    foreach ($this->plugin_types as $type) {
      $plugin = skos_importer_plugin($this->config[$type]['plugin_key'], $this->id);
      
      if($type != 'fetcher')
      {
        if (isset($this->config[$type])) {
          if($type == 'processor' && isset($this->config['processor']))
          {
            $this->config[$type] = array_merge($this->config['parser'], $this->config[$type]);
          }
          $plugin->setConfig($this->config[$type]);
        }
      }
      else
      {
        if (isset($this->config[$type]['fetchers'][$this->config[$type]['plugin_key']])) {
          $plugin->setConfig($this->config[$type]['fetchers'][$this->config[$type]['plugin_key']]);
        }
      }
      
      $this->$type = $plugin;
    }
  }

  /**
   * Report how many items *should* be created on one page load by this
   * importer.
   *
   * Note:
   *
   * It depends on whether parser implements batching if this limit is actually
   * respected. Further, if no limit is reported it doesn't mean that the
   * number of items that can be created on one page load is actually without
   * limit.
   *
   * @return
   *   A positive number defining the number of items that can be created on
   *   one page load. 0 if this number is unlimited.
   */
  public function getLimit() {
    return $this->processor->getLimit();
  }

  /**
   * Save configuration.
   */
  public function save() {
    $save = new stdClass();
    $save->id = $this->id;
    $save->config = $this->getConfig();

    if ($config = db_query("SELECT config FROM {skos_importer} WHERE id = :id", array(':id' => $this->id))->fetchField()) {
      drupal_write_record('skos_importer', $save, 'id');
      // Only rebuild menu if content_type has changed. Don't worry about
      // rebuilding menus when creating a new importer since it will default
      // to the standalone page.
      $config = unserialize($config);
      
      variable_set('menu_rebuild_needed', TRUE);
    }
    else {
      drupal_write_record('skos_importer', $save);
    }
  }

  /**
   * Load configuration and unpack.
   */
  public function load() {
    ctools_include('export');
    if ($config = ctools_export_load_object('skos_importer', 'conditions', array('id' => $this->id))) {
      $config = array_shift($config);
      $this->export_type = $config->export_type;
      $this->disabled = isset($config->disabled) ? $config->disabled : FALSE;
      $this->config = $config->config;
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Delete configuration. Removes configuration information
   * from database, does not delete configuration itself.
   */
  public function delete() {
    db_delete('skos_importer')
      ->condition('id', $this->id)
      ->execute();
    $job = array(
      'type' => $this->id,
      'id' => 0,
    );
    if ($this->export_type & EXPORT_IN_CODE) {
      skos_importer_reschedule($this->id);
    }
  }

  /**
   * Get the last import date of the importe
   */
  public function getLastImportDate() {
    if (empty($this->imported)) {
      $this->imported = db_query("SELECT last_import FROM {skos_importer} WHERE id = :id", array(':id' => $this->id))->fetchField();
    }
    return $this->imported;
  }

  /**
   * Set plugin.
   *
   * @param $plugin_key
   *   A fetcher, parser or processor plugin.
   *
   * @todo Error handling, handle setting to the same plugin.
   */
  public function setPlugin($plugin_key) {
    // $plugin_type can be either 'fetcher', 'parser' or 'processor'
    if ($plugin_type = SKOSImporterPlugin::typeOf($plugin_key)) {
      if ($plugin = skos_importer_plugin($plugin_key, $this->id)) {
        // Unset existing plugin, switch to new plugin.
        unset($this->$plugin_type);
        $this->$plugin_type = $plugin;
        // Set configuration information, blow away any previous information on
        // this spot.
        $this->config[$plugin_type] = array('plugin_key' => $plugin_key);
      }
    }
  }

  /**
   * Copy a SKOSImporterImporter configuration into this importer.
   *
   * @param SKOSImporterImporter $importer
   *   The SKOS Importer importer object to copy from.
   */
   public function copy(SKOSImporterConfigurable $configurable) {
     parent::copy($configurable);

     if ($configurable instanceof SKOSImporterImporter) {
       // Instantiate new fetcher, parser and processor and initialize their
       // configurations.
       foreach ($this->plugin_types as $plugin_type) {
         $this->setPlugin($configurable->config[$plugin_type]['plugin_key']);
         $this->$plugin_type->setConfig($configurable->config[$plugin_type]['config']);
       }
     }
   }

  /**
   * Return defaults for SKOS importer configuration.
   */
  public function configDefaults() {
  
    $defaultConfigArray = array(
      'name' => '',
      'description' => '',
      'fetcher' => array(
        'plugin_key' => 'SKOSImporterHTTPFetcher',
      ),
      'parser' => array(
        'plugin_key' => 'SKOSImporterTermParser',
        'multi_language' => 0,
        'languages' => array(),
        'additional_data' => array('altLabel' => 0, 'hiddenLabel' => 0, 'scopeNote' => 0, 'related' => 0),
        'import_type' => SKOSIMPORTER_IMPORT_INTO_MANY_VOCABURARIES,
        'vocabulary_name' => '',
      ),
      'processor' => array(
        'plugin_key' => 'SKOSImporterTermProcessor',
        'update_existing' => SKOSIMPORTER_SKIP_EXISTING,
      ),
      'import_period' => 86400, // Refresh every 1 day by default.
      'process_in_background' => FALSE,
    );
  
    $fetchers = SKOSImporterPlugin::byType('fetcher');

    if(!empty($fetchers))
    {
      foreach ($fetchers as $key => $fetcher) {
        $fetcher = skos_importer_plugin($fetcher['handler']['class'], $this->id);
        $defaultConfigArray['fetcher']['fetchers'][$key] = $fetcher->configDefaults();
      }
    }
    
    return $defaultConfigArray;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $config = $this->getConfig();
    $form = array();
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('A human readable name of this importer.'),
      '#default_value' => $config['name'],
      '#required' => TRUE,
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#description' => t('A description of this importer.'),
      '#default_value' => $config['description'],
    );
    $form['parser']['plugin_key'] = array(
      '#type' => 'value',
      '#value' => $config['parser']['plugin_key'],
      '#parents' => array('parser', 'plugin_key'),
    );
    $form['parser']['additional_data'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Additional data to import'),
      '#description' => t('Additional data to save in the vocabulary if existent'),
      '#parents' => array('parser', 'additional_data'),
      '#default_value' => $config['parser']['additional_data'],
      '#options' => array('altLabel' => t('alternative label'), 'hiddenLabel' => t('hidden label'), 'scopeNote' => t('scope note'), 'related' => t('has related')),
    );
    $form['parser']['import_type'] = array(
      '#type' => 'radios',
      '#title' => t('How are the ConceptSchemes to be imported'),
      '#parents' => array('parser', 'import_type'),
      '#options' => array(
        SKOSIMPORTER_IMPORT_INTO_MANY_VOCABURARIES => t('For every ConceptScheme create a new vocabulary'),
        SKOSIMPORTER_IMPORT_INTO_ONE_VOCABURARY => t('Import all ConceptSchemes and Concepts in one vocabulary'),
      ),
      '#default_value' => $config['parser']['import_type'],
      '#required' => TRUE,
    );
    $form['parser']['vocabulary_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name of the Vocabulary'),
      '#parents' => array('parser', 'vocabulary_name'),
      '#default_value' => $config['parser']['vocabulary_name'],
      '#states' => array(
        'visible' => array(   // action to take.
          ':input[name="parser[import_type]"]' // element to evaluate condition on
            => array('value' => SKOSIMPORTER_IMPORT_INTO_ONE_VOCABURARY),  // condition
        ),
        'required' => array(   // action to take.
          ':input[name="parser[import_type]"]' // element to evaluate condition on
            => array('value' => SKOSIMPORTER_IMPORT_INTO_ONE_VOCABURARY),  // condition
        ),
      ),
    );
    
    $form['processor']['plugin_key'] = array(
      '#type' => 'value',
      '#value' => $config['processor']['plugin_key'],
      '#parents' => array('processor', 'plugin_key'),
    );
    $form['processor']['update_existing'] = array(
      '#type' => 'radios',
      '#title' => t('Update existing terms'),
      '#description' =>
        t('Existing terms will be determined using mappings that are a "unique target".'),
      '#options' => array(
        SKOSIMPORTER_SKIP_EXISTING => t('Do not update existing terms'),
        SKOSIMPORTER_UPDATE_EXISTING => t('Update existing terms'),
      ),
      '#parents' => array('processor', 'update_existing'),
      '#default_value' => $config['processor']['update_existing'],
    );
    
    $fetchers = SKOSImporterPlugin::byType('fetcher');

    if(!empty($fetchers))
    {
      $form['fetcher'] = array(
        '#type' => 'fieldset',
        '#title' => 'Fetcher',
      );
      
      foreach ($fetchers as $key => $fetcher) {
        $form['fetcher']['plugin_key'][$key] = array(
          '#type' => 'radio',
          '#parents' => array('fetcher', 'plugin_key'),
          '#title' => check_plain($fetcher['name']),
          '#description' => filter_xss(isset($fetcher['help']) ? $fetcher['help'] : $fetcher['description']),
          '#return_value' => $key,
          '#default_value' => ($fetcher['handler']['class'] == $config['fetcher']['plugin_key']) ? $key : '',
        );
      }
      
      foreach ($fetchers as $key => $fetcher) {
      
        $fetcherObject = skos_importer_plugin($fetcher['handler']['class'], $this->id);
        $fetcherObject->setConfig($config['fetcher']['fetchers'][$key]);
        $fetcherConfigForm = $fetcherObject->configForm($form_state);
      
        if(!empty($fetcherConfigForm))
        {
          $form['fetcher']['fetchers'][$key] = array(
            '#type' => 'fieldset',
            '#title' => 'Configuration ' . $fetcher['name'],
            '#states' => array(
              'visible' => array(   // action to take.
                ':input[name="fetcher[plugin_key]"]' // element to evaluate condition on
                  => array('value' => $key),  // condition
              ),
            ),
          );
        }
        
        $form = array_merge_recursive($form, $fetcherConfigForm);
      }
    }
    
    $languages_options = array();
    foreach (language_list() as $languageKey => $language) {
      $languages_options[$languageKey] = t($language->name);
    }
    
    $default_language = language_default('language');
    
    $form['parser']['multi_language'] = array(
      '#type' => 'checkbox',
      '#title' => t('Multiple languages'),
      '#description' => t('Choose if multiple languages should be imported (if not, the terms will be imported language-neutral)'),
      '#parents' => array('parser', 'multi_language'),
      '#default_value' => $config['parser']['multi_language']
    );
    
    $form['parser']['languages'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Languages to import'),
      '#description' => t('Languages that should be imported (term-translation)'),
      '#parents' => array('parser', 'languages'),
      '#default_value' => ((isset($config['parser']['languages'])) ? $config['parser']['languages'] : array($default_language)),
      '#options' => $languages_options,
      '#states' => array(
        'visible' => array(
          ':input[name="parser[multi_language]"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="parser[multi_language]"]' => array('checked' => TRUE),
        ),
      ),
    );
    
    $form['parser']['language_mapping'] = array(
      '#type' => 'fieldset',
      '#title' => t('Language mapping'),
    );
    
    $form['parser']['language_mapping'][LANGUAGE_NONE] = array(
        '#type' => 'textfield',
        '#title' => t('Default Language mapping', array('%lang' => $lang_name)),
        '#description' => t('The iso-code of the language in the result of the fetcher to use as the default'),
        '#default_value' => ((isset($config['parser']['language_mapping']) && isset($config['parser']['language_mapping'][LANGUAGE_NONE])) ? $config['parser']['language_mapping'][LANGUAGE_NONE] : $default_language),
        '#parents' => array('parser', 'language_mapping', LANGUAGE_NONE),
      );
    
    foreach ($languages_options as $lang_key => $lang_name) {
      $form['parser']['language_mapping'][$lang_key] = array(
        '#type' => 'textfield',
        '#title' => t('Language mapping for language "%lang"', array('%lang' => $lang_name)),
        '#description' => t('The iso-code of the language in the result of the fetcher'),
        '#default_value' => ((isset($config['parser']['language_mapping']) && isset($config['parser']['language_mapping'][$lang_key])) ? $config['parser']['language_mapping'][$lang_key] : $lang_key),
        '#parents' => array('parser', 'language_mapping', $lang_key),
        '#states' => array(
          'visible' => array(
            ':input[name="parser[languages][' . $lang_key . ']"]' => array('checked' => TRUE),
          ),
        ),
      );
    }
    
    $cron_required =  ' ' . l(t('Requires cron to be configured.'), 'http://drupal.org/cron', array('attributes' => array('target' => '_new')));
    $period = drupal_map_assoc(array(900, 1800, 3600, 10800, 21600, 43200, 86400, 259200, 604800, 2419200), 'format_interval');
    foreach ($period as &$p) {
      $p = t('Every !p', array('!p' => $p));
    }
    $period = array(
      SKOSIMPORTER_SCHEDULE_NEVER => t('Off'),
      0 => t('As often as possible'),
    ) + $period;
    $form['import_period'] = array(
      '#type' => 'select',
      '#title' => t('Periodic import'),
      '#options' => $period,
      '#description' => t('Choose how often a source should be imported periodically.') . $cron_required,
      '#default_value' => $config['import_period'],
    );
    $form['process_in_background'] = array(
      '#type' => 'checkbox',
      '#title' => t('Process in background'),
      '#description' => t('For very large imports. If checked, import and delete tasks started from the web UI will be handled by a cron task in the background rather than by the browser. This does not affect periodic imports, they are handled by a cron task in any case.') . $cron_required,
      '#default_value' => $config['process_in_background'],
    );
    return $form;
  }
  
  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {
    if(!empty($values['fetcher']['plugin_key']) && isset($values['fetcher']['fetchers'][$values['fetcher']['plugin_key']]))
    {
      $fetcherObject = skos_importer_plugin($values['fetcher']['plugin_key'], $this->id);
      $fetcherObject->configFormValidate($values['fetcher']['fetchers'][$values['fetcher']['plugin_key']]);
    }
  }

  /**
   * Reschedule if import period changes.
   */
  public function configFormSubmit(&$values) {
    if ($this->config['import_period'] != $values['import_period']) {
      skos_importer_reschedule($this->id);
    }
    parent::configFormSubmit($values);
  }
}

/**
 * Helper, see SKOSImporterDataProcessor class.
 */
function skos_importer_format_expire($timestamp) {
  if ($timestamp == SKOSIMPORTER_EXPIRE_NEVER) {
    return t('Never');
  }
  return t('after !time', array('!time' => format_interval($timestamp)));
}
