<?php

/**
 * @file
 * Contains SKOSImporterProcessor and related classes.
 */

// Default limit for creating items on a page load, not respected by all
// processors.
define('SKOSIMPORTER_PROCESS_LIMIT', 50);

/**
 * Thrown if a validation fails.
 */
class SKOSImporterValidationException extends Exception {}

/**
 * Thrown if a an access check fails.
 */
class SKOSImporterAccessException extends Exception {}

/**
 * Abstract class, defines interface for processors.
 */
abstract class SKOSImporterProcessor extends SKOSImporterPlugin {
  /**
   * @defgroup entity_api_wrapper Entity API wrapper.
   */

  /**
   * Create a new entity.
   *
   * @param $source
   *   The SKOS source that spawns this entity.
   *
   * @return
   *   A new entity object.
   */
  protected abstract function newTerm(SKOSImporterSource $source);

  /**
   * Load an existing entity.
   *
   * @param $source
   *   The SKOS source that spawns this entity.
   * @param $entity_id
   *   The unique id of the entity that should be loaded.
   *
   * @return
   *   A new entity object.
   */
  protected abstract function loadTerm(SKOSImporterSource $source, $entity_id);

  /**
   * Validate an entity.
   *
   * @throws SKOSImporterValidationException $e
   *   If validation fails.
   */
  protected function termValidate($entity) {}

  /**
   * Save an entity.
   *
   * @param $entity
   *   Entity to be saved.
   */
  protected abstract function saveTerm($entity);

  /**
   * Delete a series of entities.
   *
   * @param $entity_ids
   *   Array of unique identity ids to be deleted.
   */
  protected abstract function entityDeleteMultiple($entity_ids);

  /**
   * @}
   */

  /**
   * Process the result of the parsing stage.
   *
   * @param SKOSImporterSource $source
   *   Source information about this import.
   * @param SKOSImporterParserResult $parser_result
   *   The result of the parsing stage.
   */
  public function process(SKOSImporterSource $source, SKOSImporterParserResult $parser_result, $state) {
    if (!isset($state[SKOSIMPORTER_PROCESS])) {
      $state[SKOSIMPORTER_PROCESS] = $source->state(SKOSIMPORTER_PROCESS);
    }
    $term_sets = array();

    while ($item = $parser_result->shiftItem()) {

      // Check if this item already exists.
      $tid = $this->existingTermId($source, $parser_result);
      $this->preProcessItem($source, $item, $tid);

      $skip_existing = $this->config['update_existing'] == SKOSIMPORTER_SKIP_EXISTING;

      // If it exists, and we are not updating, pass onto the next item.
      if ($tid && $skip_existing) {
        continue;
      }

      $hash = $this->hash($item);
      $changed = ($hash !== $this->getHash($tid));
      $force_update = $this->config['skip_hash_check'];

      // Do not proceed if the item exists, has not changed, and we're not
      // forcing the update.
      if ($tid && !$changed && !$force_update) {
        continue;
      }

      try {

        // Build a new term.
        if (empty($tid)) {
          $term = $this->newTerm($source);
          $this->newItemInfo($term, $hash);
        }

        // Load an existing term.
        else {
          $term = $this->loadTerm($source, $tid);

          // The skos_importer_terms table is always updated with the info for the most recently processed term.
          // The only carryover is the tid.
          $this->newItemInfo($term, $hash);
          $term->skos_importer_terms->tid = $tid;
        }

        // Set property and field values.
        $this->map($source, $parser_result, $term);
        $this->termValidate($term);

        // Allow modules to alter the term before saving.
        module_invoke_all('skos_importer_presave', $source, $term, $item);
        if (module_exists('rules')) {
          rules_invoke_event('skos_importer_import_'. $source->importer()->id, $term);
        }

        // This will throw an exception on failure.
        $this->saveTerm($term);

        // Track progress.
        if (empty($tid)) {
          $state[SKOSIMPORTER_PROCESS]->created++;
        }
        else {
          $state[SKOSIMPORTER_PROCESS]->updated++;
        }
      }

      // Something bad happened, log it.
      catch (Exception $e) {
        $state[SKOSIMPORTER_PROCESS]->failed++;
        drupal_set_message($e->getMessage(), 'warning');
        $message = $e->getMessage();
        $message .= '<h3>Original item</h3>';
        $message .= '<pre>' . var_export($item, TRUE) . '</pre>';
        $message .= '<h3>Term</h3>';
        $message .= '<pre>' . var_export($term, TRUE) . '</pre>';
        $source->log('import', $message, WATCHDOG_ERROR);
      }
    }
    
    return $this->buildProcessResult($state);

    // Set messages if we're done.
    /*if ($source->progressImporting() != SKOSIMPORTER_BATCH_COMPLETE) {
      return;
    }*/
  }
  
  protected function preProcessItem(SKOSImporterSource $source, &$result_item, $tid = 0) {}
  
  protected function buildProcessResult($state) {
    return array(
      'state' => $state,
    );
  }
  
  public function finishProcessing($source, $state) {
    $state = $state[SKOSIMPORTER_PROCESS];
    
    $messages = array();
    if ($state->created) {
      $messages[] = array(
       'message' => format_plural(
          $state->created,
          'Created @number term.',
          'Created @number terms.',
          array('@number' => $state->created)
        ),
      );
    }
    if ($state->updated) {
      $messages[] = array(
       'message' => format_plural(
          $state->updated,
          'Updated @number term.',
          'Updated @number terms.',
          array('@number' => $state->updated)
        ),
      );
    }
    if ($state->failed) {
      $messages[] = array(
       'message' => format_plural(
          $state->failed,
          'Failed importing @number term.',
          'Failed importing @number terms.',
          array('@number' => $state->failed)
        ),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new terms.'),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }

  /**
   * Remove all stored results or stored results up to a certain time for a
   * source.
   *
   * @param SKOSImporterSource $source
   *   Source information for this expiry. Implementers should only delete items
   *   pertaining to this source.
   */
  public function clear(SKOSImporterSource $source) {
    $state = $source->state(SKOSIMPORTER_PROCESS_CLEAR);

    // Build base select statement.
    $select = db_select('taxonomy_term_data', 'e');
    $select->addField('e', 'tid', 'tid');
    $select->join(
      'skos_importer_terms',
      'fi',
      "e.tid = fi.tid");
    $select->condition('fi.importer_id', $this->id);

    // If there is no total, query it.
    if (!$state->total) {
      $state->total = $select->countQuery()
        ->execute()
        ->fetchField();
    }

    // Delete a batch of entities.
    $entities = $select->range(0, $this->getLimit())->execute();
    $tids = array();
    foreach ($entities as $entity) {
      $tids[$entity->tid] = $entity->tid;
    }
    $this->entityDeleteMultiple($tids);
    
    

    // Report progress, take into account that we may not have deleted as
    // many items as we have counted at first.
    if (count($tids)) {
      $state->deleted += count($tids);
      $state->progress($state->total, $state->deleted);
    }
    else {
      $state->progress($state->total, $state->total);
    }

    // Report results when done.
    if ($source->progressClearing() == SKOSIMPORTER_BATCH_COMPLETE) {
      if ($state->deleted) {
        $message = format_plural(
          $state->deleted,
          'Deleted @number term',
          'Deleted @number terms',
          array(
            '@number' => $state->deleted,
          )
        );
        $source->log('clear', $message, WATCHDOG_INFO);
        drupal_set_message($message);
      }
      else {
        drupal_set_message(t('There are no terms to be deleted.'));
      }
    }
  }

  /*
   * Report number of items that can be processed per call.
   *
   * 0 means 'unlimited'.
   *
   * If a number other than 0 is given, SKOS Importer parsers that support batching
   * will only deliver this limit to the processor.
   *
   * @see SKOSImporterSource::getLimit()
   * @see SKOSImporterCSVParser::parse()
   */
  public function getLimit() {
    return variable_get('skos_importer_process_limit', SKOSIMPORTER_PROCESS_LIMIT);
  }

  /**
   * Delete vocabulary items younger than now - $time. Do not invoke expire on a
   * processor directly, but use SKOSImporterImporter::expire() instead.
   *
   * @see SKOSImporterImporter::expire().
   * @see SKOSImporterDataProcessor::expire().
   *
   * @param $time
   *   If implemented, all items produced by this configuration that are older
   *   than REQUEST_TIME - $time should be deleted.
   *   If $time === NULL processor should use internal configuration.
   *
   * @return
   *   SKOSIMPORTER_BATCH_COMPLETE if all items have been processed, a float between 0
   *   and 0.99* indicating progress otherwise.
   */
  public function expire($time = NULL) {
    return SKOSIMPORTER_BATCH_COMPLETE;
  }

  /**
   * Counts the number of items imported by this processor.
   */
  public function termCount(SKOSImporterSource $source) {
    return db_query("SELECT count(*) FROM {skos_importer_terms} WHERE importer_id = :importer_id", array(':importer_id' => $this->id))->fetchField();
  }

  /**
   * Execute mapping on an item.
   *
   * This method encapsulates the central mapping functionality. When an item is
   * processed, it is passed through map() where the properties of $source_item
   * are mapped onto $term following the processor's mapping
   * configuration.
   *
   * For each mapping SKOSImporterParser::getSourceElement() is executed to retrieve
   * the source element, then SKOSImporterProcessor::setTargetElement() is invoked
   * to populate the target item properly. Alternatively a
   * hook_x_targets_alter() may have specified a callback for a mapping target
   * in which case the callback is asked to populate the target item instead of
   * SKOSImporterProcessor::setTargetElement().
   *
   * @ingroup mappingapi
   *
   * @see hook_skos_importer_parser_sources_alter()
   * @see hook_skos_importer_data_processor_targets_alter()
   * @see hook_skos_importer_node_processor_targets_alter()
   * @see hook_skos_importer_term_processor_targets_alter()
   * @see hook_skos_importer_user_processor_targets_alter()
   */
  protected function map(SKOSImporterSource $source, SKOSImporterParserResult $result, $term) {

    // Static cache $targets as getMappingTargets() may be an expensive method.
    static $sources;
    if (!isset($sources[$this->id])) {
      $sources[$this->id] = skos_importer_importer($this->id)->parser->getMappingSources();
    }
    static $targets;
    if (!isset($targets[$this->id])) {
      $targets[$this->id] = $this->getMappingTargets();
    }
    $parser = skos_importer_importer($this->id)->parser;
    if (empty($term)) {
      $term = new stdClass();
    }

    // Many mappers add to existing fields rather than replacing them. Hence we
    // need to clear target elements of each item before mapping in case we are
    // mapping on a prepopulated item such as an existing node.
    foreach ($this->config['mappings'] as $mapping) {
      if (isset($targets[$this->id][$mapping['target']]['real_target'])) {
        unset($term->{$targets[$this->id][$mapping['target']]['real_target']});
      }
      elseif (isset($term->{$mapping['target']})) {
        unset($term->{$mapping['target']});
      }
    }

    /*
    This is where the actual mapping happens: For every mapping we envoke
    the parser's getSourceElement() method to retrieve the value of the source
    element and pass it to the processor's setTargetElement() to stick it
    on the right place of the target item.

    If the mapping specifies a callback method, use the callback instead of
    setTargetElement().
    */
    
    // Initialize the languages if not already done (for batches).
    if (!isset($parser->language_default)) {
      $parser->initLanguages();
    }

    foreach ($this->config['mappings'] as $mapping) {
      // Retrieve source element's value from parser.
      if (isset($sources[$this->id][$mapping['source']]) &&
          is_array($sources[$this->id][$mapping['source']]) &&
          isset($sources[$this->id][$mapping['source']]['callback']) &&
          function_exists($sources[$this->id][$mapping['source']]['callback'])) {
        $callback = $sources[$this->id][$mapping['source']]['callback'];
        $value = $callback($source, $result, $mapping['source']);
      }
      else {
        $value = $parser->getSourceElement($source, $result, $mapping['source']);
      }

      // Map the source element's value to the target.
      if (isset($targets[$this->id][$mapping['target']]) &&
          is_array($targets[$this->id][$mapping['target']]) &&
          isset($targets[$this->id][$mapping['target']]['callback']) &&
          function_exists($targets[$this->id][$mapping['target']]['callback'])) {
        $callback = $targets[$this->id][$mapping['target']]['callback'];
        $callback($source, $term, $mapping['target'], $value, $mapping);
      }
      else {
        $this->setTargetElement($source, $term, $mapping['target'], $value, $parser->language_default);
      }
    }
    return $term;
  }

  /**
   * Per default, don't support expiry. If processor supports expiry of imported
   * items, return the time after which items should be removed.
   */
  public function expiryTime() {
    return SKOSIMPORTER_EXPIRE_NEVER;
  }

  /**
   * Declare default configuration.
   */
  public function configDefaults() {
    return array(
      'mappings' => array(),
      'update_existing' => SKOSIMPORTER_SKIP_EXISTING,
      'input_format' => NULL,
      'skip_hash_check' => FALSE,
    );
  }

  /**
   * Overrides parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();
    $form['update_existing'] = array(
      '#type' => 'radios',
      '#title' => t('Update existing terms'),
      '#description' =>
        t('Existing terms will be determined using mappings that are a "unique target".'),
      '#options' => array(
        SKOSIMPORTER_SKIP_EXISTING => t('Do not update existing terms'),
        SKOSIMPORTER_UPDATE_EXISTING => t('Update existing terms'),
      ),
      '#default_value' => $this->config['update_existing'],
    );
    global $user;
    $formats = filter_formats($user);
    foreach ($formats as $format) {
      $format_options[$format->format] = $format->name;
    }
    $form['skip_hash_check'] = array(
      '#type' => 'checkbox',
      '#title' => t('Skip hash check'),
      '#description' => t('Force update of items even if item source data did not change.'),
      '#default_value' => $this->config['skip_hash_check'],
    );
    $form['input_format'] = array(
      '#type' => 'select',
      '#title' => t('Text format'),
      '#description' => t('Select the input format for the body field of the nodes to be created.'),
      '#options' => $format_options,
      '#default_value' => isset($this->config['input_format']) ? $this->config['input_format'] : 'plain_text',
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * Get mappings.
   */
  public function getMappings() {
    return isset($this->config['mappings']) ? $this->config['mappings'] : array();
  }

  /**
   * Declare possible mapping targets that this processor exposes.
   *
   * @ingroup mappingapi
   *
   * @return
   *   An array of mapping targets. Keys are paths to targets
   *   separated by ->, values are TRUE if target can be unique,
   *   FALSE otherwise.
   */
  public function getMappingTargets() {
    return array();
  }

  /**
   * Set a concrete target element. Invoked from SKOSImporterProcessor::map().
   *
   * @ingroup mappingapi
   */
  public function setTargetElement(SKOSImporterSource $source, $target_item, $target_element, $value) {
    switch ($target_element) {
      case 'url':
      case 'guid':
        $target_item->skos_importer_item->$target_element = $value;
        break;
      default:
        $target_item->$target_element = $value;
        break;
    }
  }

  /**
   * Retrieve the target entity's existing id if available. Otherwise return 0.
   *
   * @ingroup mappingapi
   *
   * @param SKOSImporterSource $source
   *   The source information about this import.
   * @param $result
   *   A SKOSImporterParserResult object.
   *
   * @return
   *   The serial id of an entity if found, 0 otherwise.
   */
  protected function existingTermId(SKOSImporterSource $source, SKOSImporterParserResult $result) {
    $query = db_select('skos_importer_terms')
      ->fields('skos_importer_terms', array('tid'))
      ->condition('importer_id', $source->id);

    // Iterate through all unique targets and test whether they do already
    // exist in the database.
    foreach ($this->uniqueTargets($source, $result) as $target => $value) {
      switch ($target) {
        case 'url':
          $entity_id = $query->condition('url', $value)->execute()->fetchField();
          break;
        case 'guid':
          $entity_id = $query->condition('guid', $value)->execute()->fetchField();
          break;
      }
      if (isset($entity_id)) {
        // Return with the content id found.
        return $entity_id;
      }
    }
    return 0;
  }


  /**
   * Utility function that iterates over a target array and retrieves all
   * sources that are unique.
   *
   * @param $batch
   *   A SKOSImporterImportBatch.
   *
   * @return
   *   An array where the keys are target field names and the values are the
   *   elements from the source item mapped to these targets.
   */
  protected function uniqueTargets(SKOSImporterSource $source, SKOSImporterParserResult $result) {
    $parser = skos_importer_importer($this->id)->parser;
    $targets = array();
    foreach ($this->config['mappings'] as $mapping) {
      if ($mapping['unique']) {
        // Invoke the parser's getSourceElement to retrieve the value for this
        // mapping's source.
        $targets[$mapping['target']] = $parser->getSourceElement($source, $result, $mapping['source']);
      }
    }
    return $targets;
  }

  /**
   * Adds SKOSImporter specific information on $entity->skos_importer_item.
   *
   * @param $entity
   *   The entity object to be populated with new item info.
   * @param $hash
   *   The fingerprint of the source item.
   */
  protected function newItemInfo(&$term, $hash = '') {
    $term->skos_importer_terms = new stdClass();
    $term->skos_importer_terms->tid = 0;
    $term->skos_importer_terms->importer_id = $this->id;
    $term->skos_importer_terms->imported = REQUEST_TIME;
    $term->skos_importer_terms->hash = $hash;
    $term->skos_importer_terms->uri = '';
    $term->skos_importer_terms->vid = 0;
  }

  /**
   * Create MD5 hash of item and mappings array.
   *
   * Include mappings as a change in mappings may have an affect on the item
   * produced.
   *
   * @return Always returns a hash, even with empty, NULL, FALSE:
   *  Empty arrays return 40cd750bba9870f18aada2478b24840a
   *  Empty/NULL/FALSE strings return d41d8cd98f00b204e9800998ecf8427e
   */
  protected function hash($item) {
    static $serialized_mappings;
    if (!$serialized_mappings) {
      $serialized_mappings = serialize($this->config['mappings']);
    }
    return hash('md5', serialize($item) . $serialized_mappings);
  }

  /**
   * Retrieves the MD5 hash of $entity_id from the database.
   *
   * @return string
   *   Empty string if no item is found, hash otherwise.
   */
  protected function getHash($tid) {

    if ($hash = db_query("SELECT hash FROM {skos_importer_terms} WHERE tid = :tid", array(':tid' => $tid))->fetchField()) {
      // Return with the hash.
      return $hash;
    }
    return '';
  }
}
