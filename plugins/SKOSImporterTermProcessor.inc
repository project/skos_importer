<?php

/**
 * @file
 * SKOSImporterTermProcessor class.
 */

/**
 * SKOS Importer processor plugin. Create taxonomy terms from SKOS concepts.
 */
class SKOSImporterTermProcessor extends SKOSImporterProcessor {

  protected $uriMappings = array();

  /**
   * Get id of an existing vocabulary term if available.
   */
  protected function existingTermId(SKOSImporterSource $source, SKOSImporterParserResult $result) {
    $query = db_select('skos_importer_terms')
      ->fields('skos_importer_terms', array('tid'))
      ->condition('importer_id', $source->id);

    // Iterate through all unique targets and test whether they do already
    // exist in the database.
    foreach ($this->uniqueTargets($source, $result) as $target => $value) {
      switch ($target) {
        case 'vid':
          $query->condition('vid', $value);
          break;

        case 'field_uri':
          $query->condition('uri', $value);
          break;
      }
    }
    $tid = $query->execute()->fetchField();
    if (isset($tid)) {
      // Return with the content id found.
      return $tid;
    }
    return 0;
  }
  
  protected function preProcessItem(SKOSImporterSource $source, &$result_item, $tid = 0) {
    if ($tid) {
      $this->uriMappings[$result_item['uri']] = $tid;
    }
  }
  
  protected function buildProcessResult($state) {
    $return = parent::buildProcessResult($state);
    
    $return['uriMappings'] = $this->uriMappings;
    
    return $return;
  }

  /**
   * Creates a new term in memory and returns it.
   */
  /*protected function newTerm(SKOSImporterSource $source) {
    $vocabulary = $this->vocabulary();
    $term = new stdClass();
    $term->vid = $vocabulary->vid;
    $term->vocabulary_machine_name = $vocabulary->machine_name;
    $term->format = filter_fallback_format();
    return $term;
  }*/
  protected function newTerm(SKOSImporterSource $source) {
    $term = new stdClass();
    $term->format = isset($this->config['input_format']) ? $this->config['input_format'] : filter_fallback_format();
    return $term;
  }

  /**
   * Loads an existing term.
   */
  protected function loadTerm(SKOSImporterSource $source, $tid) {
    return taxonomy_term_load($tid);
  }

  /**
   * Validates a term.
   */
  protected function termValidate(&$term) {
    if (empty($term->name)) {
      // Check if there is a localize-object if no name is set.
      if (isset($term->localized )&& !empty($term->localized->name)) {
        $term->name = $term->localized->name[0]['value'];
      }
      else {
        throw new SKOSImporterValidationException(t('Term name missing (URI: %uri).' , array('%uri' => $term->uri)));
      }
    }
    if (empty($term->field_uri)) {
      throw new SKOSImporterValidationException(t('Concept URI missing (Name: %name).', array('%name' => $term->name)));
    }
  }

  /**
   * Saves a term.
   *
   * We de-array parent fields with only one item.
   * This stops leftandright module from freaking out.
   */
  protected function saveTerm($term) {
    if (isset($term->parent) && !empty($term->parent)) {
      if (isset($term->uri) && in_array($term->uri, $term->parent)) {
        throw new SKOSImporterValidationException(t("A term can't be its own child. uri:@uri", array('@uri' => $term->uri)));
      }
      $term->parent = $this->getTaxonomyIds($term->parent);
    }
    taxonomy_term_save($term);
    
    // Translation of the term if required.
    if (skos_importer_use_translation()) {
      $this->taxonomy_term_localized_save($term);
    }
    
    $this->uriMappings[$term->uri] = $term->tid;
  }


  protected function taxonomy_term_localized_save($term) {
    if (isset($term->localized)) {
      foreach ($term->localized as $field => $localizations) {
        if (property_exists($term, $field)) {
          foreach ($localizations as $localization) {
            i18n_string_translation_update(array('taxonomy', 'term', $term->tid, $field), $localization['value'], $localization['lang'], $term->$field);
          }
        }
      }
    }
  }

  /**
   * Deletes a series of terms.
   */
  protected function entityDeleteMultiple($tids) {
    foreach ($tids as $tid) {
      taxonomy_term_delete($tid);
    }
  }

  public function process(SKOSImporterSource $source, SKOSImporterParserResult $parser_result, $state = NULL) {
    $this->setMappings();
    return parent::process($source, $parser_result, $state);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'update_existing' => '1',
    ) + parent::configDefaults();
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $options = array(0 => t('Select a vocabulary'));
    foreach (taxonomy_get_vocabularies() as $vocab) {
      $options[$vocab->machine_name] = $vocab->name;
    }
    $form = parent::configForm($form_state);
    $form['vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Import to vocabulary'),
      '#description' => t('Choose the vocabulary to import into. <strong>CAUTION:</strong> when deleting terms through the "Delete items" tab, SKOS Importer will delete <em>all</em> terms from this vocabulary.'),
      '#options' => $options,
      '#default_value' => $this->config['vocabulary'],
    );
    return $form;
  }

  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {
    if (empty($values['vocabulary'])) {
      form_set_error('vocabulary', t('Choose a vocabulary'));
    }
  }

  /**
   * Override setTargetElement to operate on a target item that is a taxonomy term.
   */
  public function setTargetElement(SKOSImporterSource $source, $term, $target_element, $value, $lang) {
    if ($value != SKOSIMPORTER_EMPTY_VALUE) {
      switch ($target_element) {
        case 'vid':
          $term->skos_importer_terms->vid = $value;
          $term->vid = $value;
          break;

        case 'field_uri':
          $term->skos_importer_terms->uri = $value;
          $term->field_uri[LANGUAGE_NONE][0]['value'] = $value;
          $term->uri = $value;
          break;

        case 'field_alt_labels':
          $term->field_alt_labels[LANGUAGE_NONE][0]['value'] = $value;
          break;

        case 'name':
        case 'description':
          foreach ($value as $data) {
            
            if ($data['lang'] == $lang) {
              $term->$target_element = $value[$lang]['value'];
            }
            else {
              if (!isset($term->localized->$target_element)) {
                $term->localized->$target_element = array();
              }
              $term->localized->{$target_element}[] = $data;
            }
          }
          break;

        default:
          $term->$target_element = $value;
          break;
      }
    }
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();
    $targets += array(
      'vid' => array(
        'name'        => t('Taxonomy ID'),
        'description' => t('The taxonomy ID in which the concept is created')
      ),
      'field_uri' => array(
        'name'        => t('Concept URI'),
        'description' => t('The URI from the concept')
      ),
      'name' => array(
        'name'        => t('Name'),
        'description' => t('The name of the taxonomy term')
      ),
      'field_alt_labels' => array(
        'name'        => t('Synonyms'),
        'description' => t('The synonyms of the taxonomy term')
      ),
      'description' => array(
        'name'        => t('Description'),
        'description' => t('The description of the taxonomy term')
      ),
      'parent' => array(
        'name'        => t('Parent'),
        'description' => t('The parents of the taxonomy term')
      ),
    );

    return $targets;
  }
  
  protected function setMappings() {
    $mappings[] = array(
      'source'  => 'vid',
      'target'  => 'vid',
      'unique'  => 1
    );
    $mappings[] = array(
      'source'  => 'uri',
      'target'  => 'field_uri',
      'unique'  => 1
    );
    $mappings[] = array(
      'source'  => 'pref_label',
      'target'  => 'name',
      'unique'  => 0
    );
    $mappings[] = array(
      'source'  => 'alt_labels',
      'target'  => 'field_alt_labels',
      'unique'  => 0
    );
    $mappings[] = array(
      'source'  => 'definition',
      'target'  => 'description',
      'unique'  => 0
    );
    $mappings[] = array(
      'source'  => 'broader',
      'target'  => 'parent',
      'unique'  => 0
    );
    $this->config['mappings'] = $mappings;
  }
  
  public function getUriMappings() {
    return $this->uriMappings;
  }
  
  public function setUriMappings($mappings) {
    $this->uriMappings = $mappings;
  }
  
  protected function getTaxonomyIds($uris) {
    $tids = array();
    foreach ($uris as $uri) {
      if (isset($this->uriMappings[$uri])) {
        $tids[] = $this->uriMappings[$uri];
      }
    }

    return $tids;
  }

  /**
   * Return vocabulary to map to.
   */
  public function vocabulary() {
    if (isset($this->config['vocabulary_name'])) {
      if ($vocabulary = taxonomy_vocabulary_machine_name_load($this->config['vocabulary_name'])) {
        return $vocabulary;
      }
    }
    throw new Exception(t('No vocabulary defined for Taxonomy Term processor.'));
  }
}
