<?php

/**
 * @file
 * Home of the SKOSImporterFileFetcher and related classes.
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * SKOSImporterFileFetcher.
 */
class SKOSImporterFileFetcherResult extends SKOSImporterFetcherResult {
  /**
   * Constructor.
   */
  public function __construct($file_path) {
    parent::__construct('');
    $this->file_path = $file_path;
  }

  /**
   * Overrides parent::getRaw();
   */
  public function getRaw() {
    return $this->sanitizeRaw(file_get_contents($this->file_path));
  }

  /**
   * Overrides parent::getFilePath().
   */
  public function getFilePath() {
    if (!file_exists($this->file_path)) {
      throw new Exception(t('File @filepath is not accessible.', array('@filepath' => $this->file_path)));
    }
    return $this->sanitizeFile($this->file_path);
  }
}

/**
 * Fetches data via HTTP.
 */
class SKOSImporterFileFetcher extends SKOSImporterFetcher {

  /**
   * Implements SKOSImporterFetcher::fetch().
   */
  public function fetch(SKOSImporterSource $source) {

    // Just return a file fetcher result if this is a file.
    if (is_file($this->config['source'])) {
      return new SKOSImporterFileFetcherResult($this->config['source']);
    }

    // Batch if this is a directory.
    $state = $source->state(SKOSIMPORTER_FETCH);
    $files = array();
    if (!isset($state->files)) {
      $state->files = $this->listFiles($this->config['source']);
      $state->total = count($state->files);
    }
    if (count($state->files)) {
      $file = array_shift($state->files);
      $state->progress($state->total, $state->total - count($state->files));
      return new SKOSImporterFileFetcherResult($file);
    }

    throw new Exception(t('Resource is not a file or it is an empty directory: %source', array('%source' => $this->config['source'])));
  }

  /**
   * Return an array of files in a directory.
   *
   * @param $dir
   *   A stream wreapper URI that is a directory.
   *
   * @return
   *   An array of stream wrapper URIs pointing to files. The array is empty
   *   if no files could be found. Never contains directories.
   */
  protected function listFiles($dir) {
    $dir = file_stream_wrapper_uri_normalize($dir);
    $files = array();
    if ($items = @scandir($dir)) {
      foreach ($items as $item) {
        if (is_file("$dir/$item") && strpos($item, '.') !== 0) {
          $files[] = "$dir/$item";
        }
      }
    }
    return $files;
  }

  /**
   * Source form.
   */
  /*public function sourceForm($source_config) {
    $form = array();
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => empty($source_config['fid']) ? 0 : $source_config['fid'],
    );
    if (empty($this->config['direct'])) {
      $form['source'] = array(
        '#type' => 'value',
        '#value' => empty($source_config['source']) ? '' : $source_config['source'],
      );
      $form['upload'] = array(
        '#type' => 'file',
        '#title' => empty($this->config['direct']) ? t('File') : NULL,
        '#description' => empty($source_config['source']) ? t('Select a file from your local system.') : t('Select a different file from your local system.'),
        '#theme' => 'skos_importer_upload',
        '#file_info' => empty($source_config['fid']) ? NULL : file_load($source_config['fid']),
        '#size' => 10,
      );
    }
    else {
      $form['source'] = array(
        '#type' => 'textfield',
        '#title' => t('File'),
        '#description' => t('Specify a path to a file or a directory. Path must start with @scheme://', array('@scheme' => file_default_scheme())),
        '#default_value' => empty($source_config['source']) ? '' : $source_config['source'],
      );
    }
    return $form;
  }*/

  /**
   * Override parent::sourceFormValidate().
   */
  /*public function sourceFormValidate(&$values) {
    $values['source'] = trim($values['source']);

    $skos_importer_dir = 'public://skos_importer';
    file_prepare_directory($skos_importer_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

    // If there is a file uploaded, save it, otherwise validate input on
    // file.
    // @todo: Track usage of file, remove file when removing source.
    if ($file = file_save_upload('skos_importer', array('file_validate_extensions' => array(0 => $this->config['allowed_extensions'])), $skos_importer_dir)) {
      $values['source'] = $file->uri;
      $values['file'] = $file;
    }
    elseif (empty($values['source'])) {
      form_set_error('skos_importer][source', t('Upload a file first.'));
    }
    // If a file has not been uploaded and $values['source'] is not empty, make
    // sure that this file is within Drupal's files directory as otherwise
    // potentially any file that the web server has access to could be exposed.
    elseif (strpos($values['source'], file_default_scheme()) !== 0) {
      form_set_error('skos_importer][source', t('File needs to reside within the site\'s file directory, its path needs to start with @scheme://.', array('@scheme' => file_default_scheme())));
    }
  }*/

  /**
   * Override parent::sourceSave().
   */
  public function sourceSave(SKOSImporterSource $source) {

    // If a new file is present, delete the old one and replace it with the new
    // one.
    if (isset($this->config['file'])) {
      $file = $this->config['file'];
      if (isset($this->config['fid'])) {
        $this->deleteFile($this->config['fid']);
      }
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'skos_importer', get_class($this), $file->fid);

      $this->config['fid'] = $file->fid;
      unset($this->config['file']);
    }
  }

  /**
   * Override parent::sourceDelete().
   */
  public function sourceDelete(SKOSImporterSource $source) {
    if (isset($this->config['fid'])) {
      $this->deleteFile($this->config['fid']);
    }
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'fid' => 0,
      'upload' => '',
      'source' => '',
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();
    
    $form['fetcher']['fetchers'][get_class($this)]['fid'] = array(
      '#type' => 'value',
      '#value' => empty($this->config['fid']) ? 0 : $this->config['fid'],
      '#parents' => array('fetcher', 'fetchers', get_class($this), 'fid'),
    );
    $form['fetcher']['fetchers'][get_class($this)]['source'] = array(
      '#type' => 'value',
      '#value' => empty($this->config['source']) ? '' : $this->config['source'],
      '#parents' => array('fetcher', 'fetchers', get_class($this), 'source'),
    );
    $form['fetcher']['fetchers'][get_class($this)]['upload'] = array(
      '#type' => 'file',
      '#title' => empty($this->config['direct']) ? t('File') : NULL,
      '#description' => empty($this->config['source']) ? t('Select a file from your local system.') : t('Select a different file from your local system.'),
      '#theme' => 'skos_importer_upload',
      '#file_info' => empty($this->config['fid']) ? NULL : file_load($this->config['fid']),
      '#size' => 10,
      '#parents' => array('fetcher', 'fetchers', get_class($this), 'upload'),
    );
    $form['fetcher']['fetchers'][get_class($this)]['previous_uploaded_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Uploaded file'),
      '#default_value' => empty($this->config['source']) ? '' : basename($this->config['source']),
      '#disabled' => TRUE,
    );
    return $form;
  }
  
  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {
  $values['source'] = trim($values['source']);

    $skos_importer_dir = 'public://skos_importer';
    file_prepare_directory($skos_importer_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

    // If there is a file uploaded, save it, otherwise validate input on
    // file.
    // @todo: Track usage of file, remove file when removing source.
    if ($file = file_save_upload('fetcher', array('file_validate_extensions' => array(0 => 'rdf')), $skos_importer_dir)) {
      $values['source'] = $file->uri;
      $values['file'] = $file;
    }
    elseif (empty($values['source'])) {
      form_set_error('skos_importer][source', t('Upload a file first.'));
    }
    // If a file has not been uploaded and $values['source'] is not empty, make
    // sure that this file is within Drupal's files directory as otherwise
    // potentially any file that the web server has access to could be exposed.
    elseif (strpos($values['source'], file_default_scheme()) !== 0) {
      form_set_error('skos_importer][source', t('File needs to reside within the site\'s file directory, its path needs to start with @scheme://.', array('@scheme' => file_default_scheme())));
    }
  }

  /**
   * Helper. Deletes a file.
   */
  protected function deleteFile($fid) {
    if ($file = file_load($fid)) {
      file_usage_delete($file, 'skos_importer', get_class($this), $fid);
      file_delete($file);
    }
  }
}
