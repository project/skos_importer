<?php

/**
 * @file
 * Home of the SKOSImporterHTTPFetcher and related classes.
 */

/**
 * Result of SKOSImporterHTTPFetcher::fetch().
 */
class SKOSImporterHTTPFetcherResult extends SKOSImporterFetcherResult {
  protected $url;
  protected $file_path;

  /**
   * Constructor.
   */
  public function __construct($url = NULL) {
    $this->url = $url;
    parent::__construct('');
  }

  /**
   * Overrides SKOSImporterFetcherResult::getRaw();
   */
  public function getRaw() {
    skos_importer_include_library('http_request.inc', 'http_request');
    $result = http_request_get($this->url);
    if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
    }
    return $this->sanitizeRaw($result->data);
  }
}

/**
 * Fetches data via HTTP.
 */
class SKOSImporterHTTPFetcher extends SKOSImporterFetcher {

  /**
   * Implements SKOSImporterFetcher::fetch().
   */
  public function fetch(SKOSImporterSource $source) {
    return new SKOSImporterHTTPFetcherResult($this->config['source']);
  }

  /**
   * Clear caches.
   */
  public function clear(SKOSImporterSource $source) {
    $url = $this->config['source'];
    skos_importer_include_library('http_request.inc', 'http_request');
    http_request_clear_cache($url);
  }

  /**
   * Implements SKOSImporterFetcher::request().
   */
  public function request() {
    skos_importer_dbg($_GET);
    @skos_importer_dbg(file_get_contents('php://input'));
    try {
      skos_importer_source($this->id)->existing()->import();
    }
    catch (Exception $e) {
      // In case of an error, respond with a 503 Service (temporary) unavailable.
      header('HTTP/1.1 503 "Not Found"', NULL, 503);
      drupal_exit();
    }
    // Will generate the default 200 response.
    header('HTTP/1.1 200 "OK"', NULL, 200);
    drupal_exit();
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'source' => '',
    );
  }
  
  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();

    $form['fetcher']['fetchers'][get_class($this)]['source'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('Enter an URL of a SKOS file in RDF/XML-format.'),
      '#default_value' => isset($this->config['source']) ? $this->config['source'] : '',
      '#maxlength' => NULL,
      //'#required' => TRUE,
      '#parents' => array('fetcher', 'fetchers', get_class($this), 'source'),
    );
    return $form;
  }
  
  /**
   * Override parent::sourceFormValidate().
   */
  public function configFormValidate(&$values) {
    $values['source'] = trim($values['source']);

    if (!skos_importer_valid_url($values['source'], TRUE)) {
      $form_key = 'fetcher][fetchers][' . get_class($this) . '][source';
      form_set_error($form_key, t('The URL %source is invalid.', array('%source' => $values['source'])));
    }
  }
}
